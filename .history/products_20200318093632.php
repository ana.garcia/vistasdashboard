<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <LINK REL=StyleSheet HREF="styles.css" TYPE="text/css" MEDIA=screen>
    <link href="https://fonts.googleapis.com/css?family=Prompt:300,400&display=swap" rel="stylesheet">
    <title>Document</title>
</head>
<body>
 
    <div class="main">
     <div class="header_main">
            <div class="font_size_14_gray">Consola Usuario</div>
            <div class="helo_name">
                <h1 class="hello">!Hola Daniel¡</h1>
                <h4 class="wellcome">Bienvenido a consola</h4>
            </div>
            <div class="repor_diary">
                <h4 class="report">Tu reporte diario</h4>
                <div class="repor_diary repor-line">
                    <div class="box_icon"></div>
                    <h2 class="sold font_color_black">514</h2>
                    <div class="font_size_14_gray">Productos Comprados</div>
                </div>
                <div class="repor_diary repor-line ">
                    <div class="arrow_icon"></div>
                    <h2 class="sold font_color_black">45</h2>
                    <div class="font_size_14_gray">Pedidos por dia promedio</div>
                </div>
            </div>
        </div>
        <div class="main_products">
            <div class="report-nav">
                <div class="repor_diary">
                    <h4 class="last_promo">Últimos pedidos</h4>
                    <div class="a">Mira y modifica los pedidos que has realizado.</div>
               </div>
                <div class="promo_report">
                    <h4 class="a">Promociones disponibles</h4>
                    <div class="show">Ver ventas</div>
                   </div>
                
            </div>
            
            <div class="products">
            
                <div class="card_product">
                    <div class="img_product"></div>
                    <div class="card_info_general">
                        <div class="info_product_card">
                            <a class="font_11 font_color_gray">Licores</a>
                            <h3>Santa Teresa Gran Reserva</h3>
                            <p class="font_11 ">Botella de ron Santa Teresa 700 ml</p>
                        </div>
                       <div class="info_product font_11 font_bold">
                           <p>1 Producto</p>
                           <p>15/03/2020 de 08:00am a 10:00am</p>
                           <p>Cll 00 # 00 - 00 Int 4 Apto 302</p>
                       </div>
                    </div>
                   <div class="a">
                   <div class="price">
                    <div class="float_right">  <p class="a">Precio</p> </div>
                    <div class="float_right"><h3 class="a">$860.000 BS</h3></div>
                   </div>
                    <div class="btn_show_buy">Ver esta compra</div>
                    <div class="btn_edit_delate font_bold">
                        <div class="edit">
                            <div class=""></div>
                            <div class="a">Editar</div>
                        </div>
                        <div class="delate">
                            <div class="icon_delate"></div>
                            <div class="a">Eliminar</div>
                        </div>
                    </div>
                   </div>
                </div>
                
                <div class="card_product">
                    <div class="discount">
                        <div class="dcto"><span class="font_bold font_12">30%</span> dcto</div>
                    </div>
                    <div class="img_product"></div>
                    <div class="card_info_general">
                        <div class="info_product_card">
                            <a class="font_11 font_color_gray">Licores</a>
                            <h3>Santa Teresa Gran Reserva</h3>
                            <p class="font_11">Botella de ron Santa Teresa 700 ml</p>
                        </div>
                       <div class="info_product font_11 font_bold">
                           <p>1 Producto</p>
                           <p>15/03/2020 de 08:00am a 10:00am</p>
                           <p>Cll 00 # 00 - 00 Int 4 Apto 302</p>
                       </div>
                    </div>
                   <div class="a">
                   <div class="price">
                    <div class="float_right">  <p class="a">Precio</p> </div>
                    <div class="float_right"><h3 class="a">$860.000 BS</h3></div>
                   </div>
                    <div class="btn_show_buy">Ver esta compra</div>
                    <div class="btn_edit_delate font_bold">
                        <div class="edit">
                            <div class="a"></div>
                            <div class="a">Editar</div>
                        </div>
                        <div class="delate">
                            <div class="a"></div>
                            <div class="a">Eliminar</div>
                        </div>
                    </div>
                   </div>
                </div>
                <div class="card_product">
                    <div class="img_product"></div>
                    <div class="card_info_general">
                        <div class="info_product_card">
                            <a class="font_11 font_color_gray">Licores</a>
                            <h3>Santa Teresa Gran Reserva</h3>
                            <p class="font_11">Botella de ron Santa Teresa 700 ml</p>
                        </div>
                       <div class="info_product font_11 font_bold">
                           <p>1 Producto</p>
                           <p>15/03/2020 de 08:00am a 10:00am</p>
                           <p>Cll 00 # 00 - 00 Int 4 Apto 302</p>
                       </div>
                    </div>
                   <div class="a">
                   <div class="price">
                    <div class="float_right"> <p class="a">Precio</p></div>
                    <div class="float_right"><h3 class="a">$860.000 BS</h3></div>
                   </div>
                    <div class="btn_show_buy">Ver esta compra</div>
                    <div class="btn_edit_delate font_bold">
                        <div class="edit">
                            <div class="a"></div>
                            <div class="a">Editar</div>
                        </div>
                        <div class="delate">
                            <div class="a"></div>
                            <div class="a">Eliminar</div>
                        </div>
                    </div>
                   </div>
                </div>
             </div>
             <div class="btn_show_more">Ver Mas</div>
           </div>
           
    </div>
    
</body>
</html>