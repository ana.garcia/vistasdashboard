<script>
  document.write(
    '<script src="http://' +
      (location.host || '${1:localhost}').split(':')[0] +
      ':${2:35729}/livereload.js?snipver=1"></' +
      'script>'
  );
</script>


<div class="main">
    <div class="header_main">
        <div class="buy_modify">
            <div class="space">Compras programadas</div>
        <div class="font_size_14_gray">Modificar entrega</div>
        </div>
        <div class="title">
            <h1 class="hello">Modificar entrega</h1>
         </div>
    <div class="nav_modify">
        <div class="report-nav ">
            <div class="repor_diary line_modify ">
                <div class="btn_regrace">
                    <div class="iconRegrece"></div>
                    <h4 class="regrace">Regresar</h4>
                </div>
                <div class="a">Modifica la fecha y hora de tu entrega</div>
                <div class="flex-end">aaaa</div>
           </div>
         </div>
     </div>
     <div class="main_modify">
        <div class="a">
           <div class="display-flex content-dropdown">
               <div class="a">Elige la fecha de en</div>
               <select class="dropdown">
                  <option class="item-dropdown">15 / 03 / 2020</option>
                  <option class="item-dropdown">14 / 03 / 2020</option>
                  <option class="item-dropdown">13 / 03 / 2020</option>
                  <option class="item-dropdown">12 / 03 / 2020</option>
                </select>
               
           </div>
          <div class="hour_modify">
            <div class="display-flex ">
                <div class="display-flex justify-content">
                    <div class="display-flex">
                        <label class="checkbox_container">
                            <input type="checkbox"/>
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <div class="icon_clock"></div>
                    <div class="a">08:00 am - 10:00 am</div>
                </div>
                <div class="display-flex">
                    <div class="display-flex justify-content">
                        <label class="checkbox_container">
                            <input type="checkbox"/>
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <div class="icon_clock"></div>
                    <div class="a">08:00 am - 10:00 am</div>
                </div>
            </div>
            <div class="display-flex">
                <div class="display-flex justify-content">
                    <div class="display-flex">
                        <label class="checkbox_container">
                            <input type="checkbox"/>
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <div class="icon_clock"></div>
                    <div class="a">08:00 am - 10:00 am</div>
                </div>
                <div class="display-flex">
                    <div class="display-flex justify-content">
                        <label class="checkbox_container">
                            <input type="checkbox"/>
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <div class="icon_clock"></div>
                    <div class="a">08:00 am - 10:00 am</div>
                </div>
            </div>
            <div class="display-flex">
                <div class="display-flex justify-content">
                    <div class="display-flex">
                        <label class="checkbox_container">
                            <input type="checkbox"/>
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <div class="icon_clock"></div>
                    <div class="a">08:00 am - 10:00 am</div>
                </div>
                <div class="display-flex">
                    <div class="display-flex justify-content">
                        <label class="checkbox_container">
                            <input type="checkbox"/>
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <div class="icon_clock"></div>
                    <div class="a">08:00 am - 10:00 am</div>
                </div>
            </div>
          </div>
          <div class="display-flex line_modify">
              <div class="program_servises">
                Tu servicio está programado para el:
              </div>
              <div class="font_bold space-between">
                15 / 03 / 2020 de 08:00 am a 10:00 am
              </div>
          </div>
          <div class="text_horary">
              <p>*Los horarios de entrega podrán ser en cualquier momento en el rango de hora elegida</p>
          </div>
          <div class="a">
              <div class="btn_modify">
                Modificar entrega
              </div>
          </div>
        </div>
        <div class="">
           
            <div class="">
                <div class="display-flex space-between">
                    <div class="a">a</div>
                    <div class="a">a</div>
                </div>
                <div class="display-flex space-between">
                    <div class="a">a</div>
                    <div class="a">a</div>
                </div>
                <div class="display-flex space-between">
                    <div class="a">a</div>
                    <div class="a">a</div>
                </div>
            </div>
            <div class="display-flex">
                <div class="a">a</div>
                <div class="a">a</div>
            </div>
        </div>
       
      </div>
       
</div>
