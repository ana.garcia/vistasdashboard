<header>
        <div class="header_user_console" >
            <div class="logo_header"></div>
            <div class="profile_notifications">
                <div class="item_bell">
                    <div class="">
                        <div class="icon_bell"></div>
                    </div>
                </div>
                <div class="profile-user">
                    <div class="avatar_user"></div>
                    <div class="nav-avatra_user">
                        <div class="name">name</div>
                        <div class="a">Usuario</div>
                    </div>
                    
                </div>
            </div>
        </div>
        <div class="content">
            <div class="menu_lateral font_size_14">
                <div class="sibar">
                <div class="space_menu"></div>
                    <div class="items_menu_dashboard">
                        <div class="Dashboard_icon"></div>
                        <div class="">Mi Dashboard</div>
                    </div>
                    <div class="items_menu_dashboard">
                        <div class="icon_cash"></div>
                        <div class="a">Compras programadas</div>
                    </div>
                    <div class="items_menu_dashboard">
                        <div class="icon_history"></div>
                        <div class="a">Historial</div>
                    </div>
                    <div class="items_menu_dashboard">
                        <div class="icon_user"></div>
                        <div class="a">Mi perfil</div>
                    </div>
                    <div class="items_menu_dashboard">
                        <div class="icon_gestion"></div>
                        <div class="a">Gestionar mi billetera</div>
                    </div>
                    <div class="items_menu_dashboard">
                        <div class="icon_contac"></div>
                        <div class="a">Contactar a soporte</div>
                    </div>
                    <div class="items_close_secion">
                        <div class="icon_close"></div>
                        <div class="a">Cerrar sesión</div>
                    </div>
                 </div>
            </div>
           
            <?php include 'modify.php';?> 
        </div>
        
   </header>
